#ifndef LINESTRUCT_H_
#define LINESTRUCT_H_

#include <iomanip>
#include <iostream>

#include "SDL.h"

struct line
{
  double x1;
  double y1;
  double x2;
  double y2;
  unsigned short stroke_width;
  Uint32 color;

  line(double x1, double y1, double x2, double y2, Uint32 color, unsigned short stroke_width = 1) : 
    x1(x1), 
    y1(y1), 
    x2(x2),
    y2(y2),
    stroke_width(stroke_width),
    color(color) {};
};

std::ostream& operator<<(std::ostream& out, line line_)
{
  out << std::dec
      << "X1: " << line_.x1
      << " Y1: " << line_.y1
      << " X2: " << line_.x2
      << " Y2: " << line_.y2
      << " Str_width: " << line_.stroke_width
      << " Color: 0x" << std::hex << line_.color;
  return out;
}

#endif