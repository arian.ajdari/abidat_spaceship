#include<iostream>

#include "Pitch.h"
#include "SDL.h"
#include "rclcpp/rclcpp.hpp"

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::Rate r(100);
    Pitch pitch;
    pitch.drawPitch();

    bool quit = false;
    SDL_Event event;

    while(!quit) {

        //screen.drawPoint(x++, y++);
        //if(x > 599) x =0; if(y > 799) y = 0;

        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
                quit = true;
            }
        }

        r.sleep();
    }

  

    rclcpp::shutdown();

    return 0;
}