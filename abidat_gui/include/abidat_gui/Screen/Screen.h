#ifndef SCREEN_H_
#define SCREEN_H_

#include <iostream>

#include "SDL.h"
#include "Buffer/Buffer.h"
#include "Constants/Constants.h"

class Screen
{
  protected:
    SDL_Window *window_{nullptr};
    SDL_Renderer *renderer_{nullptr};
    SDL_Texture *texture_{nullptr};

  public:
    Screen();
    bool init();
    bool processEvents();
    void update();
    void close();
};

Screen::Screen()
{
    Screen::init();
}

bool Screen::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        return false;

    window_ = SDL_CreateWindow("Abidat Football", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN::SCREEN_WIDTH, SCREEN::SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

    if (window_ == nullptr)
    {
        SDL_Quit();
        return false;
    }

    renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_PRESENTVSYNC);

    if (renderer_ == nullptr)
    {
        std::cout << "Could not create renderer" << std::endl;
        SDL_DestroyWindow(window_);
        SDL_Quit();
        return false;
    }

    texture_ = SDL_CreateTexture(renderer_, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STATIC, SCREEN::SCREEN_WIDTH, SCREEN::SCREEN_HEIGHT);

    if (texture_ == nullptr)
    {
        std::cout << "Could not create texture" << std::endl;
        SDL_DestroyRenderer(renderer_);
        SDL_DestroyWindow(window_);
        SDL_Quit();
        return false;
    }

    Buffer::fillBuffer();
    Screen::update();

    return true;
}

bool Screen::processEvents()
{
    return false;
}

void Screen::update()
{
    SDL_UpdateTexture(texture_, NULL, Buffer::getBuffer(), SCREEN::SCREEN_WIDTH * sizeof(Uint32));
    SDL_RenderClear(renderer_);
    SDL_RenderCopy(renderer_, texture_, NULL, NULL);
    SDL_RenderPresent(renderer_);
}

void Screen::close()
{
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyTexture(texture_);
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

#endif