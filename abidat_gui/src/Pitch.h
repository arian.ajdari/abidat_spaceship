#ifndef MYSCREEN_H_
#define MYSCREEN_H_

#include "Line/Line.h"
#include "Screen/Screen.h"

class Pitch : public Screen
{
  private:
    Line line;
  public:
    Pitch() = default;
    void drawPitch();
};


void Pitch::drawPitch()
{
  line.drawLine(0, 0, 0, 799, 10, 0xffffff00);
  line.drawLine(589, 0, 589, 799, 10, 0xffffff00);
  line.drawLine(0, 0, 599, 0, 10, 0xffffff00);
  line.drawLine(0, 789, 599, 789, 10, 0xffffff00);
  line.drawLine(0, 395, 599, 395, 10, 0xffffff00);
  
  Screen::update();
}



#endif