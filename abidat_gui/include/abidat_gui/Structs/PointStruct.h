#ifndef POINTSTRUCT_H_
#define POINTSTRUCT_H_

#include <iomanip>
#include <iostream>

#include "SDL.h"

struct point
{
  int x1;
  int y1;
  Uint32 color;
  point(int x1, int y1, Uint32 color) : x1(x1), y1(y1), color(color) {};

};

std::ostream& operator<<(std::ostream& out, point point_)
{
  out << std::dec
      << "X: " << point_.x1
      << " Y: " << point_.y1
      << " Color: 0x" << std::hex << point_.color;
  return out;
}

#endif