#ifndef POINT_H_
#define POINT_H_

#include <vector>
#include <iostream>

#include "Buffer/Buffer.h"
#include "Constants/Constants.h"
#include "Structs/PointStruct.h"

class Point : public Buffer
{
private:
  static std::vector<point> points;
public:
   inline void drawPoint(int row, int col, Uint32 color);
  inline void drawPoint(point& point_);
  std::vector<point> getPoints() const;
  void printPoints() const;
};

std::vector<point> Point::points{};

inline void Point::drawPoint(int row, int col,Uint32 color)
{
    points.push_back(point(row, col, color));
    buffer_[row * SCREEN::SCREEN_WIDTH + col] = color;
}

inline void Point::drawPoint(point& point_)
{
  points.push_back(std::move(point_));
  buffer_[point_.x1 * SCREEN::SCREEN_WIDTH + point_.y1] = point_.color;
}

std::vector<point> Point::getPoints() const
{
  return points;
}

void Point::printPoints() const 
{
  for(auto& point : points)
  {
    std::cout << point << std::endl;
  }
}




#endif