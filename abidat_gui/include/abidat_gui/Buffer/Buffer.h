#ifndef BUFFER_H_
#define BUFFER_H_

#include <cmath>
#include <iostream>

#include "SDL.h"
#include "Constants/Constants.h"

class Buffer
{
public:
    static Uint32 *buffer_;
public:
    Buffer() = default;
    virtual ~Buffer();
    static Uint32* getBuffer();
    static void fillBuffer();
    void fillBuffer(unsigned short color);

    Uint32 operator()(int row, int col);
};

Uint32* Buffer::buffer_ = new Uint32[SCREEN::SCREEN_WIDTH * SCREEN::SCREEN_HEIGHT];

Buffer::~Buffer()
{
    delete [] Buffer::buffer_;
}

Uint32* Buffer::getBuffer()
{
    return Buffer::buffer_;
}

void Buffer::fillBuffer()
{
    memset(Buffer::buffer_, 0, SCREEN::SCREEN_WIDTH * SCREEN::SCREEN_HEIGHT * sizeof(Uint32));
}

void Buffer::fillBuffer(unsigned short color)
{
    memset(Buffer::buffer_, color, SCREEN::SCREEN_WIDTH * SCREEN::SCREEN_HEIGHT * sizeof(Uint32));
}

Uint32 Buffer::operator()(int row, int col)
{
    return Buffer::buffer_[row * SCREEN::SCREEN_WIDTH + col];
}

#endif