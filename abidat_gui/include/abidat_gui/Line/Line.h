#ifndef LINE_H_
#define LINE_H_

#include <iostream>

#include "Point/Point.h"
#include "Structs/LineStruct.h"

class Line : public Point
{
private:
  inline double getSlope(double& x1, double& y1, double& x2, double& y2);
  void swap(double& first, double& second);
  inline int getDistance(double x1, double y1, double x2, double y2);
  inline double getYHeight(double x1, double y1, double slope);
  inline double getStep(double x1, double x2, int distance);
  bool outOfBordersCheck(double x1, double y1, double x2, double y2);
  inline void drawPoint(int row, int col, Uint32 color);

public:
  void drawLine(line line_);
  void drawLine(double x1, double y1, double x2, double y2, Uint32 color);
  void drawLine(double x1, double y1, double x2, double y2, unsigned short stroke_width, Uint32 color);
};


inline void Line::drawPoint(int row, int col, Uint32 color)
{
   buffer_[row * SCREEN::SCREEN_WIDTH + col] = color;
}

void Line::drawLine(line line_)
{
    Line::drawLine(line_.x1, line_.y1, line_.x2, line_.y2, line_.stroke_width, line_.color);
}

void Line::drawLine(double x1, double y1, double x2, double y2, Uint32 color)
{
    if(outOfBordersCheck(x1, y1, x2, y2)) return;
    if((x2 - x1) == 0)
    {
        int distance = getDistance(x1, y1, x2, y2);
        while(distance-- > 0)
            drawPoint((int)x1, (int)y1++, color);
        return;
    }

    if(x1 > x2) { swap(x1, x2); swap(y1, y2); }

    double slope = getSlope(x1, y1, x2, y2); 
    double b = getYHeight(x1, y1, slope);
    int distance = getDistance(x1, y1, x2, y2);
    double step = getStep(x1, x2, distance);

    double X_step = x1;
    double Y_step = 0;

    while(distance-- > 0)
    {      
        Y_step = (slope * X_step) + b;
        drawPoint((int)X_step, (int)Y_step, color);
        X_step += step;    
    }
}

void Line::drawLine(double x1, double y1, double x2, double y2, unsigned short stroke_width, Uint32 color)
{
    if((x2 - x1) == 0)
    {
        while(stroke_width-- > 0)
            drawLine(x1++, y1, x2++, y2, color);
        return;
    }

    while(stroke_width-- > 0)
        drawLine(x1, y1++, x2, y2++, color);
    return;
}

inline double Line::getSlope(double& x1, double& y1, double& x2, double& y2)
{
    return (y2 - y1) / (x2 - x1);
}

void Line::swap(double& first, double& second)
{
    double temp = first;
    first = second;
    second = temp;
}

inline int Line::getDistance(double x1, double y1, double x2, double y2)
{
    return (int)sqrt(pow(y2-y1, 2) + pow(x2 - x1, 2));
}

inline double Line::getYHeight(double x1, double y1, double slope)
{
    return y1 - (slope * x1);
}

inline double Line::getStep(double x1, double x2, int distance)
{
    return (double)((x2 - x1) / distance);
}

bool Line::outOfBordersCheck(double x1, double y1, double x2, double y2)
{
    if(
        x1 < 0 || y1 < 0 || x2 < 0 || y2 < 0 ||

        x1 > SCREEN::SCREEN_HEIGHT || y1 > SCREEN::SCREEN_WIDTH || 
        x2 > SCREEN::SCREEN_HEIGHT || y2 > SCREEN::SCREEN_WIDTH 
    ) return true;
    return false;
}




#endif